//
//  CurrencyCell.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 01/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

final class CurrencyCell: TableViewCell, ConfigurableCell {

    static var height: CGFloat? {
        return Constants.cellHeight
    }

    private let countryImageView = PlaceholderImageView()
    private let currencyTitleLabel = UILabel()
    private let countryTitleLabel = UILabel()
    private let textField = UITextField()

    private var viewModel: CurrencyCellViewModel?

    private let imageProcessor = SVGProcessor(
        imageSize: .init(
            width: Constants.imageViewDimension,
            height: Constants.imageViewDimension
        )
    )

    override func addSubviews() {
        super.addSubviews()
        contentView.addSubviews(countryImageView, currencyTitleLabel, countryTitleLabel, textField)
    }

    override func configureSubviews() {
        super.configureSubviews()

        currencyTitleLabel.textColor = UIColor.cur.titleColor
        currencyTitleLabel.font = UIFont.cur.font(of: Constants.titleFontSize)

        countryTitleLabel.textColor = UIColor.cur.descriptionColor
        countryTitleLabel.font = UIFont.cur.font(of: Constants.descriptionFontSize)

        countryImageView.textColor = UIColor.cur.titleColor
        countryImageView.font = UIFont.cur.boldFont(of: Constants.placeholderFontSize)
        countryImageView.backgroundColor = UIColor.cur.grayColor
        countryImageView.contentMode = .scaleAspectFill

        textField.textColor = UIColor.cur.titleColor
        textField.textAlignment = .right

        textField.delegate = self
        textField.keyboardType = .decimalPad

        countryImageView.layer.cornerRadius = Constants.imageViewDimension / 2
        countryImageView.layer.masksToBounds = true

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionTap))
        contentView.addGestureRecognizer(tapGesture)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        viewModel?.viewModelActionHandler = nil
        viewModel = nil
    }

    override func updateConstraints() {
        countryImageView.snp.remakeConstraints { make in
            make.leading.top.equalToSuperview().inset(Constants.defaultPadding)
            make.width.height.equalTo(Constants.imageViewDimension)
        }

        textField.snp.remakeConstraints { make in
            make.trailing.equalToSuperview().inset(Constants.defaultPadding)
            make.centerY.equalToSuperview()
        }

        currencyTitleLabel.snp.remakeConstraints { make in
            make.top.equalTo(countryImageView.snp.top)
            make.trailing.greaterThanOrEqualTo(textField.snp.leading).offset(Constants.defaultSpacing)
            make.leading.equalTo(countryImageView.snp.trailing).offset(Constants.defaultPadding)
        }

        countryTitleLabel.snp.remakeConstraints { make in
            make.bottom.equalTo(countryImageView.snp.bottom)
            make.trailing.greaterThanOrEqualTo(textField.snp.leading).offset(Constants.defaultSpacing)
            make.leading.equalTo(countryImageView.snp.trailing).offset(Constants.defaultPadding)
        }

        super.updateConstraints()
    }

    func configure(with viewModel: CurrencyCellViewModel) {
        self.viewModel = viewModel

        currencyTitleLabel.text = viewModel.title
        countryImageView.title = viewModel.title.first ?? Character(" ")
        textField.text = viewModel.value
        updateCountryFields()

        viewModel.viewModelActionHandler = { [weak self] action, _ in
            switch action {
            case .countryUpdated:
                self?.updateCountryFields()
            case .valueChanged(let newValue):
                self?.updateTextValue(text: newValue)
            }
        }
    }

    @objc
    private func actionTap(_ sender: UITapGestureRecognizer) {
        textField.becomeFirstResponder()
    }

    private func updateCountryFields() {
        if let viewModel = viewModel {
            countryTitleLabel.text = viewModel.description
            countryImageView.kf.setImage(with: viewModel.imageUrl, options: [.processor(imageProcessor)])
        }
    }

    private func updateTextValue(text: String) {
        if !textField.isFirstResponder {
            textField.text = text
        }
    }
}

extension CurrencyCell: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            return updatedText.count < Constants.maxTextFieldLength
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        viewModel?.perform(viewAction: .beginEditing)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel?.perform(viewAction: .endEditing(textField.text))
    }
}
private extension Constants {
    static let textFieldHeight: CGFloat = 44.0
    static let titleFontSize: CGFloat = 18.0
    static let descriptionFontSize: CGFloat = 14.0
    static let placeholderFontSize: CGFloat = 36.0
    static let textFieldFontSize: CGFloat = 22.0
    static let lineHeight: CGFloat = 2.0
    static let cellHeight: CGFloat = 84.0
    static let imageViewDimension: CGFloat = 52.0
    static let maxTextFieldLength = 12
}

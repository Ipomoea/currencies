//
//  CurrencyCellViewModel.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 01/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation

final class CurrencyCellViewModel: ViewModel, TwoWayActionable {

    enum ViewAction {
        case beginEditing
        case endEditing(String?)
    }

    enum ViewModelAction {
        case countryUpdated
        case valueChanged(String)
    }

    private(set) var currencyValue: CurrencyValue
    private let numberFormatterService: NumberFormatterService
    private let countryService: CountryService

    private(set) var imageUrl: URL?
    private(set) var description: String?

    var title: String {
        return currencyValue.currency.rawValue
    }

    private(set) var value: String {
        set { currencyValue.value = numberFormatterService.currencyValue(from: newValue) }
        get { return numberFormatterService.currencyString(from: currencyValue.value) }
    }

    var viewActionHandler: ((ViewAction, CurrencyCellViewModel) -> Void)?
    var viewModelActionHandler: ((ViewModelAction, CurrencyCellViewModel) -> Void)?

    init(currencyValue: CurrencyValue, countryService: CountryService, numberFormatterService: NumberFormatterService) {
        self.currencyValue = currencyValue
        self.numberFormatterService = numberFormatterService
        self.countryService = countryService
        updateCountryFields()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(countriesLoadedNotification),
            name: .CountryServiceDidLoadCountries,
            object: nil
        )
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc
    private func countriesLoadedNotification() {
        updateCountryFields()
    }

    private func updateCountryFields() {
        imageUrl = countryService.country(from: currencyValue.currency)?.flag
        description = countryService.countryCurrency(from: currencyValue.currency)?.name

        DispatchQueue.main.async { [weak self] in
            self?.perform(viewModelAction: .countryUpdated)
        }
    }

    func on(viewAction: CurrencyCellViewModel.ViewAction) {
        switch viewAction {
        case .beginEditing:
            break
        case .endEditing(let newValue):
            if let value = newValue {
                self.value = value
            }
        }
    }

    func on(viewModelAction: CurrencyCellViewModel.ViewModelAction) {
        switch viewModelAction {
        case .countryUpdated:
            break
        case .valueChanged(let newValue):
            self.value = newValue
        }
    }
}

extension CurrencyCellViewModel: Hashable {

    static func == (lhs: CurrencyCellViewModel, rhs: CurrencyCellViewModel) -> Bool {
        return lhs.currencyValue == rhs.currencyValue
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(currencyValue)
    }
}

//
//  CurrenciesViewModel.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 01/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation
import Alamofire

final class CurrenciesViewModel: TableViewModel {

    var reloadAction: VoidClosure?
    var scrollAction: VoidClosure?
    var loadDelayedAction: ParameterClosure<Bool>?

    private var rates: [CurrencyValue] = [] {
        didSet { updateCellViewModels() }
    }

    private(set) var currencyViewModels: [CurrencyCellViewModel] = []

    private let requestService: CurrenciesRequestService
    private let numberFormatterService: NumberFormatterService
    private let countryService: CountryService

    private var currency: Currency {
        return mainRate.currency
    }

    private var mainRate: CurrencyValue {
        didSet {
            requestService.currency = mainRate.currency
            mainRateViewModel = cellViewModel(from: mainRate)
        }
    }

    private lazy var mainRateViewModel: CurrencyCellViewModel = { [unowned self] in
        return self.cellViewModel(from: self.mainRate)
    }()

    private weak var loadDelayTimer: Timer?

    init(networkService: NetworkService, countryService: CountryService, currency: Currency) {
        self.countryService = countryService
        requestService = CurrenciesRequestService(networkService: networkService, currency: currency)
        numberFormatterService = .init()

        mainRate = CurrencyValue(currency: currency, value: Constants.startCurrencyValue)

        requestService.responseClosure = handleCurrenciesResponse
        countryService.requestCountries()

        loadDelayTimerStart()
    }

    deinit {
        requestService.responseClosure = nil
    }

    func startObserving() {
        requestService.startObserving()
    }

    func endObserving() {
        requestService.endObserving()
    }

    private func updateCellViewModels() {
        let currentCurrencies = currencyViewModels.map { $0.currencyValue.currency }
        var cellViewModels = rates
            .filter { $0.currency != mainRate.currency }
            .map { rate -> CurrencyCellViewModel in
                let viewModel: CurrencyCellViewModel
                if let index = currentCurrencies.firstIndex(of: rate.currency) {
                    viewModel = currencyViewModels[index]
                } else {
                    viewModel = cellViewModel(from: rate)
                }
                viewModel.perform(viewModelAction: .valueChanged(numberFormatterService.currencyString(from: multiplied(rate.value))))
                return viewModel
            }
        cellViewModels.insert(mainRateViewModel, at: 0)
        currencyViewModels = cellViewModels
    }

    private func cellViewModel(from rate: CurrencyValue) -> CurrencyCellViewModel {
        let viewModel = CurrencyCellViewModel(
            currencyValue: rate,
            countryService: countryService,
            numberFormatterService: numberFormatterService
        )

        viewModel.viewActionHandler = { [weak self] action, viewModel in
            switch action {
            case .beginEditing:
                self?.cellDidBeginEditing(viewModel: viewModel)
            case .endEditing(let newValue):
                if let value = newValue, let doubleValue = Double(value) {
                    self?.mainRate.value = doubleValue
                }
            }
        }
        return viewModel
    }

    private func cellDidBeginEditing(viewModel: CurrencyCellViewModel) {
        mainRate = viewModel.currencyValue
        updateCellViewModels()
        reloadAction?()
        scrollAction?()
    }

    private func multiplied(_ value: Double) -> Double {
        return value * mainRate.value
    }

    private func handleCurrenciesResponse(_ response: Result<CurrencyResponse>) {
        if !requestService.observingInProgress {
            return
        }

        switch response {
        case .success(let value):
            let rates = value.rates.sorted { $0.value < $1.value }

            DispatchQueue.main.async { [unowned self] in
                self.rates = rates
                self.reloadAction?()
                self.loadDelayTimerStart()
            }
        case .failure:
            break
        }

        if case .error = countryService.state {
            countryService.requestCountries()
        }
    }

    private func loadDelayTimerStart() {
        loadDelayedAction?(false)
        loadDelayTimer?.invalidate()
        let timer = Timer.scheduledTimer(withTimeInterval: Constants.loadDelayInterval, repeats: false) { [weak self] _ in
            self?.loadDelayedAction?(true)
        }
        loadDelayTimer = timer
    }
}

private extension Constants {
    static let startCurrencyValue: Double = 1.0
}

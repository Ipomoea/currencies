//
//  CurrenciesViewController.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 01/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit
import DeepDiff

final class CurrenciesViewController: TableViewController<CurrenciesViewModel> {

    private typealias CurrencyRow = TableRow<CurrencyCell>

    private let navigationActivityIndicator = UIActivityIndicatorView(style: .gray)
    private let centerActivityIndicator = UIActivityIndicatorView(style: .gray)

    private var currencyRows: [CurrencyRow] = [] {
        didSet { sections = [TableSection(currencyRows)] }
    }

    override var cellTypes: [UITableViewCell.Type] {
        return [CurrencyCell.self]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = UIApplication.shared.applicationName
        configureActivityIndicator()

        viewModel.startObserving()
        tableView.reloadData()

        viewModel.scrollAction = { [weak tableView] in
            tableView?.scrollToTop()
        }

        viewModel.loadDelayedAction = { [weak self] in
            self?.setLoadingState($0)
        }

        setLoadingState(true)
    }

    override func configureTableView() {
        super.configureTableView()
        tableView.keyboardDismissMode = .onDrag
    }

    override func updateViewConstraints() {
        centerActivityIndicator.snp.remakeConstraints { make in
            make.center.equalToSuperview()
        }

        super.updateViewConstraints()
    }

    override func reloadTableView(initialLoad: Bool) {
        super.reloadTableView(initialLoad: initialLoad)

        let updatedRows = viewModel.currencyViewModels.map { CurrencyRow(cellViewModel: $0) }
        let changes = diff(old: currencyRows, new: updatedRows)
        currencyRows = updatedRows

        if initialLoad {
            tableView.reloadData()
        } else if !changes.isEmpty {
            tableView.reload(
                changes: changes,
                insertionAnimation: .none,
                deletionAnimation: .none,
                replacementAnimation: .none,
                updateData: { }
            )
        }
    }

    private func configureActivityIndicator() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: navigationActivityIndicator)
        view.addSubview(centerActivityIndicator)
    }

    private func setLoadingState(_ loading: Bool) {
        let useNavigationIndicator = hasContent
        if useNavigationIndicator {
            loading ? navigationActivityIndicator.startAnimating() : navigationActivityIndicator.stopAnimating()
            centerActivityIndicator.stopAnimating()
            tableView.isHidden = false
        } else {
            loading ? centerActivityIndicator.startAnimating() : centerActivityIndicator.stopAnimating()
            tableView.isHidden = loading
            navigationActivityIndicator.stopAnimating()
        }
    }

    deinit {
        viewModel.endObserving()
    }
}

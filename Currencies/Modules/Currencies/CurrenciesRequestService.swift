//
//  CurrenciesRequestService.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 10/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation
import Alamofire

final class CurrenciesRequestService {

    var currency: Currency
    var responseClosure: ParameterClosure<Result<CurrencyResponse>>?
    private(set) var observingInProgress = false

    private let networkService: NetworkService
    private let repeatingTimer: DispatchSourceTimer

    init(networkService: NetworkService, currency: Currency) {
        self.networkService = networkService
        self.currency = currency
        self.repeatingTimer = DispatchSource.makeTimerSource()
        self.repeatingTimer.setEventHandler(handler: timerFired)
    }

    deinit {
        repeatingTimer.setEventHandler(handler: nil)

        // prevent crash
        // https://forums.developer.apple.com/thread/15902
        repeatingTimer.cancel()
        repeatingTimer.resume()
    }

    func startObserving() {
        observingInProgress = true
        makeRequest()
        repeatingTimer.resume()
    }

    func endObserving() {
        repeatingTimer.suspend()
        observingInProgress = false
    }

    private func scheduleTimer() {
        repeatingTimer.schedule(deadline: .now() + Constants.currencyRetrieveInterval)
    }

    private func timerFired() {
        makeRequest()
    }

    private func makeRequest() {
        networkService.getCurrencies(from: currency) { [weak self] in
            self?.responseClosure?($0)
            self?.scheduleTimer()
        }
    }
}

//
//  NavigationService+Currencies.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 03/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

extension NavigationService {

    func currenciesViewController(networkService: NetworkService, countryService: CountryService, currency: Currency) -> CurrenciesViewController {
        let viewModel = CurrenciesViewModel(networkService: networkService, countryService: countryService, currency: currency)
        let viewController = CurrenciesViewController(viewModel: viewModel)
        return viewController
    }
}

//
//  ConfigurableView.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 01/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

protocol ConfigurableView {

    associatedtype ViewModel: Hashable

    func configure(with viewModel: ViewModel)
}

protocol ConfigurableCell: ConfigurableView {

    static var height: CGFloat? { get }
}

extension ConfigurableCell {

    static var height: CGFloat? {
        return nil
    }
}

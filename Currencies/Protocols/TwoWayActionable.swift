//
//  TwoWayActionable.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 18/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation

protocol TwoWayActionable {

    associatedtype ViewAction
    associatedtype ViewModelAction

    var viewActionHandler: ((ViewAction, Self) -> Void)? { get set }

    var viewModelActionHandler: ((ViewModelAction, Self) -> Void)? { get set }

    func on(viewAction: ViewAction)

    func on(viewModelAction: ViewModelAction)
}

extension TwoWayActionable {

    func perform(viewAction: ViewAction) {
        on(viewAction: viewAction)
        viewActionHandler?(viewAction, self)
    }

    func perform(viewModelAction: ViewModelAction) {
        on(viewModelAction: viewModelAction)
        viewModelActionHandler?(viewModelAction, self)
    }

    func on(viewAction: ViewAction) { }

    func on(viewModelAction: ViewModelAction) { }
}

//
//  CurrenciesCoordinator.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 07/05/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import XCoordinator

enum CurrencyRoute: Route {
    case main(currency: Currency)
}

final class CurrenciesCoordinator: NavigationCoordinator<CurrencyRoute> {

    private let storage = CurrenciesStorage()

    override func prepareTransition(for route: CurrencyRoute) -> NavigationTransition {
        switch route {
        case .main(let currency):
            let viewController = NavigationService().currenciesViewController(
                networkService: storage.networkService,
                countryService: storage.countryService,
                currency: currency
            )
            return .push(viewController)
        }
    }
}

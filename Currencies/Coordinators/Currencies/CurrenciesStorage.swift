//
//  CurrenciesStorage.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 07/05/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import Foundation

final class CurrenciesStorage {

    let networkService: NetworkService

    let countryService: CountryService

    init() {
        let networkService = NetworkService()
        self.networkService = networkService
        self.countryService = CountryService(networkService: networkService)
    }
}

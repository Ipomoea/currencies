//
//  ApplicationCoordinator.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 07/05/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import XCoordinator

enum ApplicationRoute: Route {
    case currencies
}

typealias ApplicationRouter = AnyRouter<ApplicationRoute>

final class ApplicationCoordinator: NavigationCoordinator<ApplicationRoute> {

    init() {
        super.init(initialRoute: .currencies)
    }

    override func prepareTransition(for route: ApplicationRoute) -> NavigationTransition {
        switch route {
        case .currencies:
            let coordinator = CurrenciesCoordinator(initialRoute: .main(currency: Constants.startCurrency))
            return .present(coordinator)
        }
    }
}

private extension Constants {
    static let startCurrency = Currency.EUR
}

//
//  Constants.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 02/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

typealias VoidClosure = () -> Void
typealias ParameterClosure<Input> = (Input) -> Void

struct Constants {

    private init() { }

    static let currencyRetrieveInterval: TimeInterval = 1.0

    static let loadDelayInterval: TimeInterval = 3.0

    static let defaultPadding: CGFloat = 16.0

    static let defaultSpacing: CGFloat = 8.0
}

enum Error: Swift.Error {
    case coding
    case deallocated
}





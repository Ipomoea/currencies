//
//  ViewController.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 01/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

class ViewController<ViewModelT: ViewModel>: UIViewController {

    let viewModel: ViewModelT

    required init(viewModel: ViewModelT) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Storyboards are incompatible with truth and beauty")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
    }

    override func viewWillLayoutSubviews() {
        view.setNeedsUpdateConstraints()
        super.viewWillLayoutSubviews()
    }
}

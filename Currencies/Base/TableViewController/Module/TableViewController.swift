//
//  TableViewController.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 04/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit
import SnapKit

class TableViewController<TableViewModelT: TableViewModel>: ViewController<TableViewModelT>, UITableViewDataSource, UITableViewDelegate {

    let tableView = UITableView()

    var sections: [TableSection] = []

    var cellTypes: [UITableViewCell.Type] {
        return []
    }

    var hasContent: Bool {
        return !sections.flatMap { $0.cells }.isEmpty
    }

    private var isInitialLoad = true

    override func viewDidLoad() {
        configureTableViewController()
        super.viewDidLoad()
    }

    override func updateViewConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        super.updateViewConstraints()
    }

    private func configureTableViewController() {
        configureTableView()
        registerCells()
        viewModel.reloadAction = { [weak self] in
            guard let self = self else {
                return
            }

            self.reloadTableView(initialLoad: self.isInitialLoad)
            if self.isInitialLoad {
                self.isInitialLoad = false
            }
        }
    }

    private func registerCells() {
        cellTypes.forEach { tableView.registerCell($0) }
    }

    func reloadTableView(initialLoad: Bool) { }

    func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        tableView.frame = view.bounds
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].numberOfItems
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableRow = row(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: tableRow.reuseIdentifier, for: indexPath)
        tableRow.configure(cell)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return row(at: indexPath).height
    }
}

private extension TableViewController {

    func row(at indexPath: IndexPath) -> TableRowProtocol {
        return sections[indexPath.section].cells[indexPath.row]
    }
}

//
//  TableViewModel.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 04/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation

protocol TableViewModel: ViewModel {

    var reloadAction: VoidClosure? { get set }
}

//
//  CellViewModel.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 04/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

protocol TableRowProtocol {

    var reuseIdentifier: String { get }

    var cellClass: UITableViewCell.Type { get }

    var height: CGFloat { get }

    func configure(_ cell: UITableViewCell)
}

extension TableRowProtocol {

    var reuseIdentifier: String {
        return cellClass.reuseIdentifier
    }
}

//
//  TableSection.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 04/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation

final class TableSection {

    var cells: [TableRowProtocol]

    var numberOfItems: Int {
        return cells.count
    }

    init(_ cells: [TableRowProtocol]) {
        self.cells = cells
    }
}

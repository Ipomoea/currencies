//
//  TableViewCell.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 01/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    override class var requiresConstraintBasedLayout: Bool {
        return true
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    func setup() {
        selectionStyle = .none
        addSubviews()
        configureSubviews()
    }

    func addSubviews() { }

    func configureSubviews() { }
}

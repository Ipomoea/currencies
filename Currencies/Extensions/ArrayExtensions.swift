//
//  ArrayExtensions.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 16/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation

extension Array {

    mutating func replace(from fromIndex: Int, to toIndex: Int) {
        let element = self[fromIndex]
        remove(at: fromIndex)
        insert(element, at: toIndex)
    }
}

//
//  UIViewExtensions.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 08/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

extension UIView {

    func addSubviews(_ views: UIView...) {
        views.forEach { addSubview($0) }
    }

    func roundView() {
        let size = bounds.size
        let dimension = min(size.width, size.height)
        let radius = dimension / 2
        layer.cornerRadius = radius
    }
}

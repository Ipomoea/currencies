//
//  UITableViewCellExtensions.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 10/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

extension UITableViewCell {

    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

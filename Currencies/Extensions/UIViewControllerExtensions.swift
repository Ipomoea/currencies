//
//  UIViewControllerExtensions.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 03/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

extension UIViewController {

    func embedInNavigationController<T: UINavigationController>() -> T {
        return T(rootViewController: self)
    }
}

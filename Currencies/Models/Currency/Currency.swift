//
//  Currency.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 02/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation

enum Currency: String, Codable, CaseIterable {
    case EUR
    case AUD
    case BGN
    case BRL
    case CAD
    case CHF
    case CNY
    case CZK
    case DKK
    case GBP
    case HKD
    case HRK
    case HUF
    case IDR
    case ILS
    case INR
    case ISK
    case JPY
    case KRW
    case MXN
    case MYR
    case NOK
    case NZD
    case PHP
    case PLN
    case RON
    case RUB
    case SEK
    case SGD
    case THB
    case TRY
    case USD
    case ZAR
}

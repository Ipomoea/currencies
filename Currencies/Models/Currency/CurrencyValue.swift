//
//  CurrencyValue.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 02/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

struct CurrencyValue {

    let currency: Currency

    var value: Double

    init?(currencyString: String, value: Double) {
        guard let currency = Currency(rawValue: currencyString) else {
            return nil
        }
        self.init(currency: currency, value: value)
    }

    init(currency: Currency, value: Double) {
        self.currency = currency
        self.value = value
    }
}

extension CurrencyValue: Hashable {

    static func == (lhs: CurrencyValue, rhs: CurrencyValue) -> Bool {
        return lhs.currency == rhs.currency
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(currency)
    }
}

//
//  CurrencyResponse.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 02/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import SwiftDate

struct CurrencyResponse {

    let base: Currency

    let date: DateInRegion

    let rates: [CurrencyValue]
}

extension CurrencyResponse: Decodable {

    private enum CodingKeys: String, CodingKey {
        case base
        case date
        case rates
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        base = try container.decode(Currency.self, forKey: .base)
        let dateString = try container.decode(String.self, forKey: .date)
        if let formattedDate = dateString.toDate(Constants.dateFormat, region: .current) {
            date = formattedDate
        } else {
            throw Error.coding
        }

        let ratesDictionary = try container.decode(Dictionary<String, Double>.self, forKey: .rates)
        rates = ratesDictionary
            .compactMap { CurrencyValue(currencyString: $0, value: $1) }
    }
}

private extension Constants {
    static let dateFormat = "yyyy-MM-dd"
}

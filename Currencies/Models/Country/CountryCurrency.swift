//
//  CountryCurrency.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 07/01/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import Foundation

struct CountryCurrency: Decodable {

    let code: String?

    let name: String?
}

//
//  AppDelegate.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 01/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    static var shared: AppDelegate {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }
        return delegate
    }

    let applicationCoordinator = ApplicationCoordinator()

    private(set) lazy var appWindow = UIWindow(frame: UIScreen.main.bounds)

    private var router: ApplicationRouter {
        return applicationCoordinator.anyRouter
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        router.setRoot(for: appWindow)
        return true
    }
}


//
//  Appearance.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 08/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import UIKit

struct Color {

    private init() { }

    static var titleColor: UIColor {
        return .darkText
    }

    static var descriptionColor: UIColor {
        return .lightGray
    }

    static var grayColor: UIColor {
        return .lightGray
    }
}

struct Font {

    private init() { }

    static func font(of size: CGFloat) -> UIFont {
        return .systemFont(ofSize: size)
    }

    static func boldFont(of size: CGFloat) -> UIFont {
        return .boldSystemFont(ofSize: size)
    }
}

extension UIColor {

    static var cur: Color.Type {
        return Color.self
    }
}

extension UIFont {

    static var cur: Font.Type {
        return Font.self
    }
}

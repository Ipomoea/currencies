//
//  NumberFormatterService.swift
//  Currencies
//
//  Created by Pavel Lukandiy on 16/12/2018.
//  Copyright © 2018 Pavel Lukandiy. All rights reserved.
//

import Foundation

final class NumberFormatterService {

    private enum Formatter {
        case currency
    }

    private let formatters: [Formatter: NumberFormatter]

    init() {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.maximumFractionDigits = 4
        currencyFormatter.minimumFractionDigits = 1
        currencyFormatter.minimumIntegerDigits = 1
        formatters = [.currency: currencyFormatter]
    }

    private func string(from value: NSNumber, formatter: Formatter) -> String {
        return formatters[formatter]?.string(from: value) ?? ""
    }

    private func value(from string: String, formatter: Formatter) -> NSNumber {
        return formatters[formatter]?.number(from: string) ?? 0
    }

    func currencyString(from value: Double) -> String {
        return string(from: NSNumber(value: value), formatter: .currency)
    }

    func currencyValue(from string: String) -> Double {
        return value(from: string, formatter: .currency).doubleValue
    }
}

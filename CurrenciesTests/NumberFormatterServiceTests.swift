//
//  NumberFormatterServiceTests.swift
//  CurrenciesTests
//
//  Created by Pavel Lukandiy on 08/01/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import XCTest
@testable import Currencies

class NumberFormatterServiceTests: XCTestCase {

    private var service: NumberFormatterService?

    override func setUp() {
        super.setUp()
        service = .init()
    }

    override func tearDown() {
        super.tearDown()
        service = nil
    }

    func testCurrencyString() {
        guard let service = service else {
            XCTAssertNotNil(self.service)
            return
        }

        XCTAssertTrue(service.currencyString(from: 12.4153216) == "12.4153")
        XCTAssertTrue(service.currencyString(from: 254.61) == "254.61")
        XCTAssertTrue(service.currencyString(from: 00.76312) == "0.7631")
        XCTAssertTrue(service.currencyString(from: 51) == "51.0")
    }

    func testCurrencyValue() {
        guard let service = service else {
            XCTAssertNotNil(self.service)
            return
        }

        XCTAssertTrue(service.currencyValue(from: "12.4153") == 12.4153)
        XCTAssertTrue(service.currencyValue(from: "254") == 254)
        XCTAssertTrue(service.currencyValue(from: ".7631") == 0.7631)
        XCTAssertTrue(service.currencyValue(from: "51.0") == 51)
    }
}

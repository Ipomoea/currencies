//
//  ArrayExtensionsTests.swift
//  CurrenciesTests
//
//  Created by Pavel Lukandiy on 08/01/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import XCTest
@testable import Currencies

class ArrayExtensionsTests: XCTestCase {

    func testReplace() {
        var test1 = [1, 5, 13, 1]
        test1.replace(from: 2, to: 1)
        XCTAssertTrue(test1 == [1, 13, 5, 1])

        var test2 = [53, 13, 64, 64, 12]
        test2.replace(from: 3, to: 2)
        XCTAssertTrue(test2 == [53, 13, 64, 64, 12])

        var test3 = [23, 54, 753, 12, 53]
        test3.replace(from: 4, to: 0)
        XCTAssertTrue(test3 == [53, 23, 54, 753, 12])
    }
}

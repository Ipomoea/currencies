//
//  CurrenciesRequestServiceTests.swift
//  CurrenciesTests
//
//  Created by Pavel Lukandiy on 08/01/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import XCTest
@testable import Currencies

class CurrenciesRequestServiceTests: XCTestCase {

    private var service: CurrenciesRequestService?
    private let expectationTimeout: TimeInterval = 20
    private let expectedFulfillmentCount = 3

    override func setUp() {
        super.setUp()

        guard let currency = Currency.allCases.randomElement() else {
            XCTFail()
            return
        }

        let networkService = NetworkService()
        service = CurrenciesRequestService(networkService: networkService, currency: currency)
    }

    override func tearDown() {
        super.tearDown()
        service = nil
    }

    func testCurrenciesRequest() {
        guard let service = service else {
            XCTAssertNotNil(self.service)
            return
        }

        var response: CurrencyResponse?
        var error: Swift.Error?
        let requestCompletedExpectation = expectation(description: "Currencies request completed")
        requestCompletedExpectation.expectedFulfillmentCount = expectedFulfillmentCount

        service.responseClosure = {
            switch $0 {
            case .success(let value):
                response = value
            case .failure(let failureError):
                error = failureError
            }
            requestCompletedExpectation.fulfill()
        }

        service.startObserving()
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        service.endObserving()

        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }

}

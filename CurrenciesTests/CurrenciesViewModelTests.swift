//
//  CurrenciesViewModelTests.swift
//  CurrenciesTests
//
//  Created by Pavel Lukandiy on 08/01/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import XCTest
@testable import Currencies

class CurrenciesViewModelTests: XCTestCase {

    private var viewModel: CurrenciesViewModel?
    private let expectationTimeout: TimeInterval = 20
    private let expectedFulfillmentCount = 3

    override func setUp() {
        super.setUp()

        guard let currency = Currency.allCases.randomElement() else {
            XCTFail()
            return
        }

        let networkService = NetworkService()
        let countryService = CountryService(networkService: networkService)
        viewModel = CurrenciesViewModel(
            networkService: networkService,
            countryService: countryService,
            currency: currency
        )
    }

    override func tearDown() {
        super.tearDown()
        viewModel = nil
    }

    func testDataReload() {
        guard let viewModel = viewModel else {
            XCTAssertNotNil(self.viewModel)
            return
        }

        let requestCompletedExpectation = expectation(description: "Data reload completed")
        requestCompletedExpectation.expectedFulfillmentCount = expectedFulfillmentCount

        viewModel.reloadAction = {
            requestCompletedExpectation.fulfill()
        }

        viewModel.startObserving()
        waitForExpectations(timeout: expectationTimeout, handler: nil)
        viewModel.endObserving()
    }
}

//
//  NetworkServiceTests.swift
//  CurrenciesTests
//
//  Created by Pavel Lukandiy on 08/01/2019.
//  Copyright © 2019 Pavel Lukandiy. All rights reserved.
//

import XCTest
@testable import Currencies

class NetworkServiceTests: XCTestCase {

    private var service: NetworkService?
    private let expectationTimeout: TimeInterval = 20

    override func setUp() {
        super.setUp()
        service = .init()
    }

    override func tearDown() {
        super.tearDown()
        service = nil
    }

    func testGetCurrencies() {
        guard let service = service else {
            XCTAssertNotNil(self.service)
            return
        }

        var response: CurrencyResponse?
        var error: Swift.Error?
        let requestCompletedExpectation = expectation(description: "Currencies request completed")

        service.getCurrencies(from: .EUR) {
            switch $0 {
            case .success(let value):
                response = value
            case .failure(let failureError):
                error = failureError
            }
            requestCompletedExpectation.fulfill()
        }

        waitForExpectations(timeout: expectationTimeout, handler: nil)

        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }

    func testGetCountries() {
        guard let service = service else {
            XCTAssertNotNil(self.service)
            return
        }

        var response: [Country]?
        var error: Swift.Error?
        let requestCompletedExpectation = expectation(description: "Countries request completed")

        service.getCountries {
            switch $0 {
            case .success(let value):
                response = value
            case .failure(let failureError):
                error = failureError
            }
            requestCompletedExpectation.fulfill()
        }

        waitForExpectations(timeout: expectationTimeout, handler: nil)

        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }
}
